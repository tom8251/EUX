#include "framework.h"

void PushOpenPathFilenameRecently( char *acPathFilename )
{
	int	index ;
	int	start ;
	int	end ;

	for( index = 0 ; index < OPEN_PATHFILENAME_RECENTLY_MAXCOUNT ; index++ )
	{
		if( strcmp( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index] , acPathFilename ) == 0 )
			break;
	}
	if( index < OPEN_PATHFILENAME_RECENTLY_MAXCOUNT )
	{
		start = 0 ;
		end = index ;
	}
	else
	{
		start = 0 ;
		end = OPEN_PATHFILENAME_RECENTLY_MAXCOUNT - 1 ;
	}

	for( index = end - 1 ; index >= start ; index-- )
	{
		strcpy( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index+1] , g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index] );
	}
	strncpy( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[0] , acPathFilename , sizeof(g_stEditUltraMainConfig.aacOpenPathFilenameRecently[0])-1 );

	return;
}

void UpdateOpenPathFilenameRecently()
{
	HMENU		hRootMenu ;
	HMENU		hMenu_FILE ;
	HMENU		hMenu_OPENRECENTLYFILES ;
	int		count ;
	int		index ;

	BOOL		bret ;

	hRootMenu = ::GetMenu(g_hwndMainWindow) ;
	hMenu_FILE = ::GetSubMenu( hRootMenu , 0 ) ;
	hMenu_OPENRECENTLYFILES = ::GetSubMenu( hMenu_FILE , 2 ) ;
	count = ::GetMenuItemCount( hMenu_OPENRECENTLYFILES ) ;
	for( index = 0 ; index < count ; index++ )
	{
		bret = ::DeleteMenu( hMenu_OPENRECENTLYFILES , 0 , MF_BYPOSITION );
	}

	for( index = 0 ; index < OPEN_PATHFILENAME_RECENTLY_MAXCOUNT ; index++ )
	{
		if( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index][0] == '\0' )
			break;

		bret = ::AppendMenu( hMenu_OPENRECENTLYFILES , MF_POPUP|MF_STRING , IDM_FILE_OPEN_RECENTLY_HISTORY_BASE+index , g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index] );
	}

	count = ::GetMenuItemCount( hMenu_OPENRECENTLYFILES ) ;
	if( count == 0 )
	{
		bret = ::AppendMenu( hMenu_OPENRECENTLYFILES , MF_POPUP|MF_STRING , IDM_FILE_OPEN_RECENTLY_HISTORY_BASE , "���գ�" );
		SetMenuItemEnable( g_hwndMainWindow , IDM_FILE_OPEN_RECENTLY_HISTORY_BASE , FALSE );
	}

	return;
}

void SetCurrentFileTypeChecked( HWND hWnd , struct TabPage *pnodeTabPage )
{
	struct DocTypeConfig	*pstDocTypeConfig = NULL ;
	int			index ;

	for( index = 0 , pstDocTypeConfig = g_astDocTypeConfig ; pstDocTypeConfig->nDocType != DOCTYPE_END ; index++ , pstDocTypeConfig++ )
	{
		SetMenuItemChecked( hWnd , IDM_VIEW_SWITCH_FILETYPE_BASE+index , (pnodeTabPage&&(pnodeTabPage->pstDocTypeConfig==pstDocTypeConfig)) );
	}

	return;
}

void SetCurrentWindowThemeChecked( HWND hWnd )
{
	int	index ;

	for( index = 0 ; index < g_nWindowThemeCount ; index++ )
	{
		SetMenuItemChecked( hWnd , IDM_VIEW_SWITCH_STYLETHEME_BASE+index , g_pstWindowTheme==&(g_astWindowTheme[index]) );
	}

	return;
}

void SetViewTabWidthMenuText( int nMenuId )
{
	MENUITEMINFO	mii ;
	char		*pStart = NULL ;
	char		*pEnd = NULL ;
	char		acMenuText[ 256 ] ;
	char		acNewMenuText[ 256 ] ;

	memset( & mii , 0x00 , sizeof(MENUITEMINFO) );
	mii.cbSize = sizeof(MENUITEMINFO) ;
	mii.fMask = MIIM_STRING ;
	mii.dwTypeData = acMenuText ;
	mii.cch = sizeof(acMenuText) - 1 ;
	::GetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );
	pStart = strchr( acMenuText , '[' ) ;
	if( pStart )
	{
		pEnd = strchr( pStart+1 , ']' ) ;
		if( pEnd )
		{
			memset( acNewMenuText , 0x00 , sizeof(acNewMenuText) );
			snprintf( acNewMenuText , sizeof(acNewMenuText)-1 , "%.*s%d%.*s" , pStart-acMenuText+1,acMenuText , g_stEditUltraMainConfig.nTabWidth , strlen(pEnd)+1 , pEnd );
		}
	}
	mii.cch = (int)strlen(acNewMenuText) - 1 ;
	mii.dwTypeData = acNewMenuText ;
	::SetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );

	return;
}

void SetSourceCodeAutoCompletedShowAfterInputCharactersMenuText( int nMenuId )
{
	MENUITEMINFO	mii ;
	char		*p = NULL ;
	char		acMenuText[ 256 ] ;

	memset( & mii , 0x00 , sizeof(MENUITEMINFO) );
	mii.cbSize = sizeof(MENUITEMINFO) ;
	mii.fMask = MIIM_STRING ;
	mii.dwTypeData = acMenuText ;
	mii.cch = sizeof(acMenuText) - 1 ;
	::GetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );
	p = strchr( acMenuText , '[' ) ;
	if( p )
	{
		*(p+1) = (g_stEditUltraMainConfig.nAutoCompletedShowAfterInputCharacters%10) + '0' ;
	}
	mii.cch = (int)strlen(acMenuText) - 1 ;
	::SetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );

	return;
}

void SetReloadSymbolListOrTreeIntervalMenuText( int nMenuId )
{
	MENUITEMINFO	mii ;
	char		*pStart = NULL ;
	char		*pEnd = NULL ;
	char		acMenuText[ 256 ] ;
	char		acNewMenuText[ 256 ] ;

	memset( & mii , 0x00 , sizeof(MENUITEMINFO) );
	mii.cbSize = sizeof(MENUITEMINFO) ;
	mii.fMask = MIIM_STRING ;
	mii.dwTypeData = acMenuText ;
	mii.cch = sizeof(acMenuText) - 1 ;
	::GetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );
	pStart = strchr( acMenuText , '[' ) ;
	if( pStart )
	{
		pEnd = strchr( pStart+1 , ']' ) ;
		if( pEnd )
		{
			memset( acNewMenuText , 0x00 , sizeof(acNewMenuText) );
			snprintf( acNewMenuText , sizeof(acNewMenuText)-1 , "%.*s%d%.*s" , pStart-acMenuText+1,acMenuText , g_stEditUltraMainConfig.nReloadSymbolListOrTreeInterval , strlen(pEnd)+1 , pEnd );
		}
	}
	mii.cch = (int)strlen(acNewMenuText) - 1 ;
	mii.dwTypeData = acNewMenuText ;
	::SetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );

	return;
}

void UpdateAllMenus( HWND hWnd , struct TabPage *pnodeTabPage )
{
	SetMenuItemChecked( hWnd , IDM_FILE_SETREADONLY_AFTER_OPEN , g_stEditUltraMainConfig.bSetReadOnlyAfterOpenFile );
	UpdateOpenPathFilenameRecently();
	SetMenuItemChecked( hWnd , IDM_FILE_UPDATE_WHERE_SELECTTABPAGE , g_stEditUltraMainConfig.bUpdateWhereSelectTabPage );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_WINDOWS_EOLS , (g_stEditUltraMainConfig.nNewFileEols==0) );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_MAC_EOLS , (g_stEditUltraMainConfig.nNewFileEols==1) );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_UNIX_EOLS , (g_stEditUltraMainConfig.nNewFileEols==2) );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_ENCODING_UTF8 , (g_stEditUltraMainConfig.nNewFileEncoding==65001) );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_ENCODING_GB18030 , (g_stEditUltraMainConfig.nNewFileEncoding==936) );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_ENCODING_BIG5 , (g_stEditUltraMainConfig.nNewFileEncoding==950) );

	SetMenuItemChecked( hWnd , IDM_EDIT_ENABLE_AUTO_ADD_CLOSECHAR , g_stEditUltraMainConfig.bEnableAutoAddCloseChar );
	SetMenuItemChecked( hWnd , IDM_EDIT_ENABLE_AUTO_INDENTATION , g_stEditUltraMainConfig.bEnableAutoIdentation );

	SetCurrentFileTypeChecked( hWnd , pnodeTabPage );
	SetCurrentWindowThemeChecked( hWnd );
	SetMenuItemChecked( hWnd , IDM_VIEW_FILETREE , g_bIsFileTreeBarShow );
	SetViewTabWidthMenuText( IDM_VIEW_TAB_WIDTH );
	SetMenuItemChecked( hWnd , IDM_VIEW_ONKEYDOWN_TAB_CONVERT_SPACES , g_stEditUltraMainConfig.bOnKeydownTabConvertSpaces );
	SetMenuItemChecked( hWnd , IDM_VIEW_WRAPLINE_MODE , g_stEditUltraMainConfig.bWrapLineMode );
	SetMenuItemChecked( hWnd , IDM_VIEW_LINENUMBER_VISIABLE , g_stEditUltraMainConfig.bLineNumberVisiable );
	SetMenuItemChecked( hWnd , IDM_VIEW_BOOKMARK_VISIABLE , g_stEditUltraMainConfig.bBookmarkVisiable );
	SetMenuItemChecked( hWnd , IDM_VIEW_WHITESPACE_VISIABLE , g_stEditUltraMainConfig.bWhiteSpaceVisiable );
	SetMenuItemChecked( hWnd , IDM_VIEW_NEWLINE_VISIABLE , g_stEditUltraMainConfig.bNewLineVisiable );
	SetMenuItemChecked( hWnd , IDM_VIEW_INDENTATIONGUIDES_VISIABLE , g_stEditUltraMainConfig.bIndentationGuidesVisiable );

	SetReloadSymbolListOrTreeIntervalMenuText( IDM_SOURCECODE_SET_RELOAD_SYMBOLLIST_OR_TREE_INTERVAL );
	SetMenuItemChecked( hWnd , IDM_SOURCECODE_ENABLE_AUTOCOMPLETEDSHOW , g_stEditUltraMainConfig.bEnableAutoCompletedShow );
	SetSourceCodeAutoCompletedShowAfterInputCharactersMenuText( IDM_SOURCECODE_AUTOCOMPLETEDSHOW_AFTER_INPUT_CHARACTERS );
	SetMenuItemChecked( hWnd , IDM_SOURCECODE_ENABLE_CALLTIPSHOW , g_stEditUltraMainConfig.bEnableCallTipShow );

	SetMenuItemChecked( hWnd , IDM_SOURCECODE_BLOCKFOLD_VISIABLE , g_stEditUltraMainConfig.bBlockFoldVisiable );

	if( pnodeTabPage )
	{
		SetMenuItemEnable( hWnd , IDM_FILE_SAVE , pnodeTabPage && IsDocumentModified(pnodeTabPage) );
		SetMenuItemEnable( hWnd , IDM_FILE_SAVEAS , pnodeTabPage && pnodeTabPage->acFilename[0] );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_WINDOWS_EOLS , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETEOLMODE,0,0)==0) );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_MAC_EOLS , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETEOLMODE,0,0)==1) );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_UNIX_EOLS , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETEOLMODE,0,0)==2) );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_ENCODING_UTF8 , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETCODEPAGE,0,0)==65001) );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_ENCODING_GB18030 , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETCODEPAGE,0,0)==936) );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_ENCODING_BIG5 , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETCODEPAGE,0,0)==950) );

		SetMenuItemEnable( hWnd , IDM_VIEW_HEXEDIT_MODE , TRUE );
		SetMenuItemChecked( hWnd , IDM_VIEW_HEXEDIT_MODE , pnodeTabPage->bHexEditMode );
	}
	else
	{
		SetMenuItemEnable( hWnd , IDM_FILE_SAVE , FALSE );
		SetMenuItemEnable( hWnd , IDM_FILE_SAVEAS , FALSE );

		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_WINDOWS_EOLS , FALSE );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_MAC_EOLS , FALSE );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_UNIX_EOLS , FALSE );

		SetMenuItemEnable( hWnd , IDM_VIEW_HEXEDIT_MODE , FALSE );
	}

	if( pnodeTabPage == NULL )
	{
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_NEW , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_OPEN , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_SAVE , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_SAVEAS , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_SAVEALL , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_CLOSE , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_REMOTE_FILESERVERS , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_UNDO , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_REDO , FALSE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_CUT , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_COPY , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_PASTE , FALSE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FIND , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FINDPREV , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FINDNEXT , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FOUNDLIST , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_REPLACE , FALSE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_TOGGLE_BOOKMARK , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_GOTO_PREV_BOOKMARK , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_GOTO_NEXT_BOOKMARK , FALSE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_NAVIGATEBACK_PREV_IN_THIS_FILE , FALSE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_FILETREE , TRUE );
		if( g_bIsFileTreeBarShow )
		{
			::SendMessage( g_hwndToolBar , TB_CHECKBUTTON , IDM_VIEW_FILETREE , TRUE );
		}
		else
		{
			::SendMessage( g_hwndToolBar , TB_CHECKBUTTON , IDM_VIEW_FILETREE , FALSE );
		}

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_MODIFY_STYLETHEME , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_HEXEDIT_MODE , FALSE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_ZOOMOUT , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_ZOOMIN , FALSE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_ABOUT , TRUE );
	}
	else
	{
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_NEW , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_OPEN , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_SAVE , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_SAVEAS , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_SAVEALL , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_CLOSE , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_REMOTE_FILESERVERS , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_UNDO , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_REDO , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_CUT , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_COPY , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_PASTE , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FIND , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FINDPREV , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FINDNEXT , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FOUNDLIST , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_REPLACE , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_TOGGLE_BOOKMARK , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_GOTO_PREV_BOOKMARK , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_GOTO_NEXT_BOOKMARK , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_NAVIGATEBACK_PREV_IN_THIS_FILE , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_FILETREE , TRUE );
		if( g_bIsFileTreeBarShow )
		{
			::SendMessage( g_hwndToolBar , TB_CHECKBUTTON , IDM_VIEW_FILETREE , TRUE );
		}
		else
		{
			::SendMessage( g_hwndToolBar , TB_CHECKBUTTON , IDM_VIEW_FILETREE , FALSE );
		}
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_HEXEDIT_MODE , TRUE );
		if( pnodeTabPage->bHexEditMode )
		{
			::SendMessage( g_hwndToolBar , TB_CHECKBUTTON , IDM_VIEW_HEXEDIT_MODE , TRUE );
		}
		else
		{
			::SendMessage( g_hwndToolBar , TB_CHECKBUTTON , IDM_VIEW_HEXEDIT_MODE , FALSE );
		}

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_MODIFY_STYLETHEME , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_ZOOMOUT , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_ZOOMIN , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_ABOUT , TRUE );
	}

	return;
}

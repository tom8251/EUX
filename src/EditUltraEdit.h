#ifndef _H_EDITULTRA_EDIT_
#define _H_EDITULTRA_EDIT_

#include "framework.h"

int OnUndoEdit( struct TabPage *pnodeTabPage );
int OnRedoEdit( struct TabPage *pnodeTabPage );

int OnCutEdit( struct TabPage *pnodeTabPage );
int OnCopyEdit( struct TabPage *pnodeTabPage );
int OnPasteEdit( struct TabPage *pnodeTabPage );
int OnDeleteEdit( struct TabPage *pnodeTabPage );
int OnCutLineEdit( struct TabPage *pnodeTabPage );
int OnCutLineAndPasteLineEdit( struct TabPage *pnodeTabPage );
int OnCopyLineEdit( struct TabPage *pnodeTabPage );
int OnCopyLineAndPasteLineEdit( struct TabPage *pnodeTabPage );
int OnPasteLineEdit( struct TabPage *pnodeTabPage );
int OnPasteLineUpstairsEdit( struct TabPage *pnodeTabPage );
int OnDeleteLineEdit( struct TabPage *pnodeTabPage );

int OnJoinLineEdit( struct TabPage *pnodeTabPage );

int OnLowerCaseEdit( struct TabPage *pnodeTabPage );
int OnUpperCaseEdit( struct TabPage *pnodeTabPage );

int OnEditEnableAutoAddCloseChar( struct TabPage *pnodeTabPage );
int OnEditEnableAutoIdentation( struct TabPage *pnodeTabPage );

int OnEditBase64Encoding( struct TabPage *pnodeTabPage );
int OnEditBase64Decoding( struct TabPage *pnodeTabPage );
int OnEditMd5( struct TabPage *pnodeTabPage );
int OnEditSha1( struct TabPage *pnodeTabPage );
int OnEditSha256( struct TabPage *pnodeTabPage );
int OnEdit3DesCbcEncrypto( struct TabPage *pnodeTabPage );
int OnEdit3DesCbcDecrypto( struct TabPage *pnodeTabPage );

#endif
